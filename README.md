## Humans of Free Code Camp     
## Campfire Stories  

### Installation

1. Run `node server.js`
2. Go to `http://localhost:8080/index.html` in your browser

### User Stories:   
**Home Page**   
	1. I can see snippets of stories by campers  
**Login**  
	1. I can login using github  
**Create Story**  
	1. After loggin in, I can click a button that lets me create a new blog post  
	2. I can create a blog post by typing html  
	3. I can preview the html   
**View Story**  
	1. clicking on a story from the home page opens up the full story  





Please see projects tab for more information.  
